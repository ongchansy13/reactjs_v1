import axios from 'axios'
import React, { useState } from 'react'
import hand from '../../src/assets/image/ai-hand.jpg'


const FormLogin = () => {

  const [payload, setPayload] = useState({
    username: '',
    password: ''
  })


  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(payload)
    axios.post('/api/auth/login', payload, {
      headers: {
        Authorization: 'Basic aG90ZWw6aG90ZWxAMTIz'
      }
    }).then((res) => {
      console.log(res.data.data.token)
      localStorage.setItem('token', res.data.data.token)
      window.location.reload();
    }).catch((err) => {
      console.log(err)
    })
  }
  return (
    <div className=''>
      <div className='w-screen h-screen  flex items-center gap-5'>

        <div className='w-1/2'>
          <img className='size-full' src={hand} alt="" />
        </div>

        <form onSubmit={handleSubmit} className='flex flex-col gap-2 w-1/2 shadow p-10'>
          <h1 className='py-3 text-3xl font-bold text-gray-500 text-center'>Welcome</h1>
          <div className='flex flex-col gap-3 '>
            <label className='text-xl'>Enter Name</label>
            <input type="text" className='outline outline-1 p-2 outline-slate-200 ' onChange={(e) => setPayload({ ...payload, username: e.target.value })} />
          </div>
          <div className='flex flex-col gap-3 '>
            <label className='text-xl'>Enter Password</label>
            <input type="password" className='outline outline-1 p-2 outline-slate-200' onChange={(e) => setPayload({ ...payload, password: e.target.value })} />
          </div>
          <div>
            <button type='submit' className='px-5 py-2 bg-blue-500'>Submit</button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default FormLogin