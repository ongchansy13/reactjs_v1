import React from 'react'
import ProjectTable from './project/ProjectTable'
import Project from './project/Project'



const HomePage = () => {

  return (
    <div>
      <Project />
      <ProjectTable />
    </div>
  )
}

export default HomePage