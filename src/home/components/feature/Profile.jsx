import React from 'react'
import ProfileBanner from '../Profilebanner/ProfileBanner'
import AboutMe from '../Profilebanner/AboutMe'
import MyTeam from '../Profilebanner/MyTeam'

const Profile = () => {
  return (
    <div className='bg-slate-50 px-10 py-10'>
      <div>
        <h1 className='text-5xl font-bold'>Profile</h1>

        <ProfileBanner />

        <div className='grid md:grid-cols-2  gap-5 mt-10'>
          <div className='w-full h-full bg-white shadow-md'>
            <AboutMe />
          </div>
          <div className='w-full h-full bg-white shadow-md'>
            <MyTeam />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Profile