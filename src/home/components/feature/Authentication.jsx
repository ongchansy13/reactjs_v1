import React from 'react'
import { Button, Checkbox, Label, TextInput } from 'flowbite-react';

const Authentication = () => {
  return (
    <div className=' grid items-center justify-center'>
      <form className="flex max-w-md flex-col gap-4 bg-white shadow-lg p-10 mt-[50%]">
        <div>
          <div className="mb-2 block ">
            <h1 className='text-black' value="Your email">Your email </h1>
          </div>
          <TextInput id="email2" type="email" placeholder="name@gmail.com" required shadow />
        </div>
        <div>
          <div className="mb-2 block">
            <Label htmlFor="password2" value="Your password" />
          </div>
          <TextInput id="password2" type="password" required shadow />
        </div>
        <div>
          <div className="mb-2 block">
            <Label htmlFor="repeat-password" value="Repeat password" />
          </div>
          <TextInput id="repeat-password" type="password" required shadow />
        </div>
        <div className="flex items-center gap-2">
          <Checkbox id="agree" />
          <Label htmlFor="agree" className="flex">
            I agree with the&nbsp;
            <button className="text-cyan-600 hover:underline dark:text-cyan-500">
              terms and conditions
            </button>
          </Label>
        </div>
        <Button type="submit">Register new account</Button>
      </form>
    </div>
  )
}

export default Authentication