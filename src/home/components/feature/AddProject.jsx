import React, { useState } from 'react'

const AddProject = () => {

  const [address, setAddress] = useState('')
  const [contact_name, setContact_name] = useState('')
  const [desc, setDesc] = useState('')
  const [location, setLocation] = useState('')
  const [name, setName] = useState('')
  const [phone_number, setPhone_number] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault()

    fetch('https://learn-dev.hasura.app/api/rest/shops', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(
        {
          "object": {
            "address": address,
            "contact_name": contact_name,
            "desc": desc,
            "location": location,
            "name": name,
            "phone_number": phone_number
          }
        }
      )
    })
      .then((res) => {
        if (res.ok) {
          alert('Post added successfully')
          setName('')
          setAddress('')
          setContact_name('')
          setDesc('')
          setLocation('')
          setPhone_number('')
        }
      })
  }


  return (
    <>
      <div>
        <h1 className='text-center my-5 text-3xl font-semibold underline underline-offset-2'>ADD POST</h1>
        <form className='flex flex-col gap-4' onSubmit={handleSubmit}>
          <div className='flex flex-col gap-2'>
            <label className='text-xl'>Address</label>
            <input type="text" className='outline outline-1 outline-slate-300 py-3 px-4' value={address} onChange={(e) => setAddress(e.target.value)} />
          </div>
          <div className='flex flex-col gap-2'>
            <label className='text-xl'>Contact Name</label>
            <input type="text" className='outline outline-1 outline-slate-300 py-3 px-4' value={contact_name} onChange={(e) => setContact_name(e.target.value)} />
          </div>
          <div className='flex flex-col gap-2'>
            <label className='text-xl'>Description</label>
            <input type="text" className='outline outline-1 outline-slate-300 py-3 px-4' value={desc} onChange={(e) => setDesc(e.target.value)} />
          </div>
          <div className='flex flex-col gap-2'>
            <label className='text-xl'>Location</label>
            <input type="text" className='outline outline-1 outline-slate-300 py-3 px-4' value={location} onChange={(e) => setLocation(e.target.value)} />
          </div>
          <div className='flex flex-col gap-2'>
            <label className='text-xl'>Name</label>
            <input type="text" className='outline outline-1 outline-slate-300 py-3 px-4' value={name} onChange={(e) => setName(e.target.value)} />
          </div>
          <div className='flex flex-col gap-2'>
            <label className='text-xl'>Phone Number</label>
            <input type="text" className='outline outline-1 outline-slate-300 py-3 px-4' value={phone_number} onChange={(e) => setPhone_number(e.target.value)} />
          </div>

          <button className='p-5 bg-blue-500 text-white font-medium'>Add Post</button>
        </form>
      </div>
    </>
  )
}

export default AddProject