import React from "react";

const AboutMe = () => {
  return (
    <div className="p-5">
      <h1 className="text-2xl font-medium mb-5">About Me</h1>
      <h2 className="text-xl font-medium mb-5">Bio</h2>
      <p className="text-sm mb-5">
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Incidunt
        blanditiis molestias officia earum dignissimos, omnis error cupiditate
        unde neque illum, corporis iste officiis obcaecati quas sit consectetur
        quisquam fuga! Eum.
      </p>
      <h2 className="text-xl font-medium mb-3">Position</h2>
      <p className="text-md mb-5">Theme designer at Reactjs & Tailwindcss.</p>

      <div className="grid grid-cols-2 gap-5">
        <div>
          <h1 className="text-lg font-medium mb-2">PHONE</h1>
          <p className="text-md font-light">+855 973775764</p>
        </div>
        <div>
          <h1 className="text-lg font-medium mb-2">DATE OF BIRTH</h1>
          <p className="text-md font-light">11/17/2002</p>
        </div>
        <div>
          <h1 className="text-lg font-medium mb-2">EMAIL</h1>
          <p className="text-md font-light">ongchansy13@gmal.com</p>
        </div>
        <div>
          <h1 className="text-lg font-medium mb-2">LOCATION</h1>
          <p className="text-md font-light">Cambodia,Phnom Penh</p>
        </div>
      </div>
    </div>
  );
};

export default AboutMe;
