import React from 'react'
import avatar from '../../../assets/image/profile4.jpg'

const ProfileBanner = () => {
  return (
    <div className='rounded-lg bg-white mt-10 overflow-hidden shadow-md'>
      <div className='bg-gradient-to-r from-violet-500 to-fuchsia-500 h-44'></div>
      <div>
        <div className=' '>
          <div className='relative'>

            <img className='rounded-full h-32 w-32 -mt-16 ml-5 border-4 border-gray-100' src={avatar} alt="" />
            <div className='h-5 w-5 bg-green-400 rounded-full absolute bottom-0 left-24 border-2 border-white'></div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-8 h-8 absolute -top-1 left-28 text-teal-400 ">
              <path fillRule="evenodd" d="M8.603 3.799A4.49 4.49 0 0 1 12 2.25c1.357 0 2.573.6 3.397 1.549a4.49 4.49 0 0 1 3.498 1.307 4.491 4.491 0 0 1 1.307 3.497A4.49 4.49 0 0 1 21.75 12a4.49 4.49 0 0 1-1.549 3.397 4.491 4.491 0 0 1-1.307 3.497 4.491 4.491 0 0 1-3.497 1.307A4.49 4.49 0 0 1 12 21.75a4.49 4.49 0 0 1-3.397-1.549 4.49 4.49 0 0 1-3.498-1.306 4.491 4.491 0 0 1-1.307-3.498A4.49 4.49 0 0 1 2.25 12c0-1.357.6-2.573 1.549-3.397a4.49 4.49 0 0 1 1.307-3.497 4.49 4.49 0 0 1 3.497-1.307Zm7.007 6.387a.75.75 0 1 0-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 0 0-1.06 1.06l2.25 2.25a.75.75 0 0 0 1.14-.094l3.75-5.25Z" clipRule="evenodd" />
            </svg>

          </div>

          <div className='flex items-center justify-between -mt-14 ml-40 '>
            <div >
              <h1 className='text-2xl font-medium '>Ong Chansy</h1>
              <span className='font-medium text-gray-500 text-sm'>@Chansy123</span>
            </div>
            <button className='px-5 py-2 bg-white border border-blue-500 text-blue-500 rounded-lg mr-5 hidden md:block'>Edit Profile</button>
          </div>

        </div>
        <div className='flex gap-10 px-5 py-5 mt-10 border-t'>
          <div className='text-blue-500'>Overview </div>
          <div>Project</div>
          <div> Files</div>
          <div>Teams</div>
          <div>Followers</div>
          <div> Activity</div>
        </div>
      </div>
    </div>
  )
}

export default ProfileBanner