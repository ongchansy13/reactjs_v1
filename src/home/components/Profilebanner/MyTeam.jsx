import React from 'react'
import person1 from '../../../assets/image/peron1.jpg'
import person2 from '../../../assets/image/person2.jpg'
import person3 from '../../../assets/image/person3.jpg'
import person4 from '../../../assets/image/person4.jpg'

const phone = <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
  <path strokeLinecap="round" strokeLinejoin="round" d="M14.25 9.75v-4.5m0 4.5h4.5m-4.5 0 6-6m-3 18c-8.284 0-15-6.716-15-15V4.5A2.25 2.25 0 0 1 4.5 2.25h1.372c.516 0 .966.351 1.091.852l1.106 4.423c.11.44-.054.902-.417 1.173l-1.293.97a1.062 1.062 0 0 0-.38 1.21 12.035 12.035 0 0 0 7.143 7.143c.441.162.928-.004 1.21-.38l.97-1.293a1.125 1.125 0 0 1 1.173-.417l4.423 1.106c.5.125.852.575.852 1.091V19.5a2.25 2.25 0 0 1-2.25 2.25h-2.25Z" />
</svg>;

const video = <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
  <path strokeLinecap="round" strokeLinejoin="round" d="m15.75 10.5 4.72-4.72a.75.75 0 0 1 1.28.53v11.38a.75.75 0 0 1-1.28.53l-4.72-4.72M4.5 18.75h9a2.25 2.25 0 0 0 2.25-2.25v-9a2.25 2.25 0 0 0-2.25-2.25h-9A2.25 2.25 0 0 0 2.25 7.5v9a2.25 2.25 0 0 0 2.25 2.25Z" />
</svg>;

const teams = [
  {
    image: person1,
    name: 'Dianna Smiley',
    skill: 'UI / UX Designer',
    phoneIcon: phone,
    videoIcon: video,
  },
  {
    image: person2,
    name: 'Anne Brewer',
    skill: 'Senior UX Designer',
    phoneIcon: phone,
    videoIcon: video,
  },
  {
    image: person3,
    name: 'Richard Christmas',
    skill: 'Front- End Engineer',
    phoneIcon: phone,
    videoIcon: video,
  },
  {
    image: person4,
    name: 'Nicholas Binder',
    skill: 'Content Marketing Manager',
    phoneIcon: phone,
    videoIcon: video,
  },
]

const MyTeam = () => {
  return (
    <div className='p-5'>

      <h1 className='mb-5 text-xl font-medium'>MyTeam</h1>
      {
        teams.map((team, index) => {
          return (
            <div className='mb-7 last:mb-0' key={index}>
              <div className='flex items-center justify-between '>
                <div className='flex items-center gap-5'>
                  <div className='w-14 h-14'>
                    <img className='size-full rounded-full  ' src={team.image} alt="" />
                  </div>
                  <div>
                    <p>{team.name}</p>
                    <span className='text-gray-500/80 text-sm'>{team.skill}</span>
                  </div>
                </div>
                <div className='flex items-center gap-2'>
                  <div className='text-gray-700'>
                    {team.phoneIcon}
                  </div>
                  <div className='text-gray-700'>
                    {team.videoIcon}
                  </div>
                </div>
              </div>
            </div>
          )
        })
      }

    </div>
  )
}

export default MyTeam