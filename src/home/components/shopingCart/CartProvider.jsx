import React, { createContext, useReducer } from 'react'

const initialState = {
  items: [],
}

export const CartContext = createContext(initialState);

const CartProvider = ({ children }) => {

  const [state, dispatch] = useReducer(cartReducer, initialState);

  return (
    <div>

      <CartContext.Provider value={{ state, dispatch }}>
        {children}
      </CartContext.Provider>

    </div>
  )
}


export const cartReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_ITEM':
      return {
        ...state,
        items: [...state.items, action.payload],
      }
    case 'REMOVE_ITEM':
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.payload),
      }
    default:
      return state
  }
}

export default CartProvider