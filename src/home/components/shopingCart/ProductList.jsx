import React, { useContext } from 'react'
import { CartContext } from './CartProvider'

const ProductList = () => {

  const { dispatch } = useContext(CartContext);

  const products = [
    { id: 1, name: 'Product 1', price: 10 },
    { id: 2, name: 'Product 2', price: 20 },
    { id: 3, name: 'Product 3', price: 30 },
  ]

  const handleAddItem = (product) => {
    dispatch({
      type: 'ADD_ITEM',
      payload: product,
    })
  }

  return (

    <div>
      <h2>ProductList</h2>
      <ul>
        {
          products.map(product => (
            <li key={product.id} className='flex items-center gap-5'>
              <p className='p-5 bg-slate-400 mb-5'>

                <p className='mb-3'>Name:{product.name}</p>
                <p>Price:{product.price}</p>
              </p>
              <button className='ml-5 text-white rounded-lg px-2 py-1 bg-sky-500 ' onClick={() => handleAddItem(product)}>Add to Cart</button>
            </li>
          ))
        }
      </ul>
    </div>
  )
}

export default ProductList