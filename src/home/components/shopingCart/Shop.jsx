import React from 'react'
import CartProvider from './CartProvider'
import ProductList from './ProductList'
import Cart from './Cart'

const Shop = () => {
  return (
    <div>
      <CartProvider>
        <h1 className="text-2xl font-bold my-4">Shopping App</h1>
        <div className="grid grid-cols-2 gap-4">
          <ProductList />
          <Cart />
        </div>
      </CartProvider>
    </div>
  )
}

export default Shop