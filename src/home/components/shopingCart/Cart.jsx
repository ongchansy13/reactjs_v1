import React, { useContext } from "react";
import { CartContext } from "./CartProvider";

const Cart = () => {
  const { state, dispatch } = useContext(CartContext);

  const handleRemoveItem = (itemId) => {
    dispatch({
      type: "REMOVE_ITEM",
      payload: itemId,
    });
  };

  return (
    <div>
      <h2>Shoping Cart</h2>

      <ul>
        {state.items.map((item) => (
          <li key={item.id} className="flex items-center gap-5">
            <p className="p-5 bg-slate-400 mb-5">
              <p className="mb-3">Name:{item.name}</p>
              <p>Price:{item.price}</p>
            </p>
            <button
              className="ml-5 text-white rounded-lg px-2 py-1 bg-sky-500"
              onClick={() => handleRemoveItem(item.id)}
            >
              Remove
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Cart;
