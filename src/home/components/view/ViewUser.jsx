import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const ViewUser = () => {
  const { id } = useParams()
  const [users, setUsers] = useState([]);

  const renderUsers = () => {
    axios
      .get(`/api/users/${id}`)
      .then((res) => {
        console.log(res.data.data);
        setUsers(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    renderUsers();
  }, [id]);

  return (
    <div>
      <p>ViewUser{id}</p>
      <p>Name: {users.name}</p>
      <p>Email: {users.email}</p>
      <p>Phone: {users.phone}</p>
      <p>Address: {users.address}</p>
      <p>Bio: {users.bio}</p>
    </div>
  )
}

export default ViewUser