import React, { useEffect, useState } from 'react'

const FetchApi = () => {
  const [data, setData] = useState([]);
  console.log(data);
  useEffect(() => {
    fetch('https://dummyjson.com/products')
      .then(response => response.json())
      .then(data => setData(data))
  }, [])


  return (
    <div>
      {
        <div>
          {/* <h1>JSON Data:{JSON.stringify(data)}</h1> */}
          <ul>
            {
              data.products?.map(d => (
                <ul key={d.id}>
                  <li> title : {d.title}</li>
                  <li>price:{d.price}</li>
                </ul>
              ))
            }
          </ul>
        </div>


      }

    </div>
  )
}

export default FetchApi