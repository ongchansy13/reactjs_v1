import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const EditUser = () => {


  const { id } = useParams();

  const [editUser, setEditUser] = useState([])

  const [post, setPost] = useState({
    username: "",
    name: "",
    phone: "",
    email: "",
    password: "",
    adress: "",
    dateOfBirth: "",
    status: 1,
    roleId: 2,
    bio: ""
  })

  useEffect(() => {
    axios.get(`/api/users/${id}`).then(res => {
      console.log(res.data.data)
      setEditUser(res.data.data)
    })
  }, [id])

  const handleChange = (e) => {
    setPost({
      ...post,
      [e.target.name]: e.target.value
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(post)
    axios.put(`/api/users/${id}`, post).then(res => {
      console.log(res.data.data)
      window.location.reload()
    }).catch(err => console.log(err))
  }





  return (
    <div>
      <h1>Delete</h1>
      <form onSubmit={handleSubmit}>
        <div className="grid grid-cols-2 gap-10">
          <div className="flex flex-col gap-3">
            <div className="flex flex-col gap-3">
              <label className="">Username</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="username"
                value={editUser.username}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className=''>Name</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="name"
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="">Phone</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="phone"
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="">Email</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="email"
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="">Password</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="password"
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="flex flex-col gap-3">
            <div className="flex flex-col gap-3">
              <label className="">Adress</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="adress"
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="">DateOfBirth</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="dateOfBirth"
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="">Bio</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="bio"
                onChange={handleChange}
              />
            </div>
          </div>
        </div>

        <div className="mt-7 ">
          <button
            className="px-10 w-full py-3 rounded-md bg-sky-400 text-white"
            type="submit"
          >
            submit
          </button>
        </div>
      </form>
    </div>
  )
}

export default EditUser