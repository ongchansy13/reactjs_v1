import axios from 'axios';
import React, { useState } from 'react'

const AddRoll = () => {
  const [userRoll, setUserRoll] = useState({
    name: '',
    code: ''
  });

  const handleChange = (e) => {
    setUserRoll({
      ...userRoll,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(userRoll);
    axios
      .post("/api/roles", userRoll)
      .then((res) => {
        console.log(res);
        window.location.reload();
        setUserRoll(res.data.data);
      })
      .catch((err) => console.log(err));

    setUserRoll({
      name: '',
      code: ''
    });
  };

  const { name, code } = userRoll

  return (
    <div className="max-w-[900px] mx-auto mt-20 bg-black text-black p-12 shadow-md rounded-md">
      <h1 className="text-center text-xl my-5">ADD USER</h1>
      <form onSubmit={handleSubmit}>
        <div className="flex flex-col gap-3">
          <label className="text-white">Name</label>
          <input
            type="text"
            className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
            name="name"
            value={name}
            onChange={handleChange}
          />
        </div>
        <div className="flex flex-col gap-3">
          <label className="text-white">Code</label>
          <input
            type="text"
            className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
            name="code"
            value={code}
            onChange={handleChange}
          />
        </div>
        <div className="mt-7 ">
          <button
            className="px-10 w-full py-3 rounded-md bg-sky-400 text-white"
            type="submit"
          >
            submit
          </button>
        </div>
      </form>
    </div>
  )
}

export default AddRoll