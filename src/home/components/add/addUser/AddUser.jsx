import React, { useState } from "react";
import axios from "axios";

const AddUser = () => {
  const [user, setUser] = useState({
    username: "",
    name: "",
    phone: "",
    email: "",
    password: "",
    adress: "",
    dateOfBirth: "",
    status: 1,
    roleId: 2,
    bio: "",
  });

  const handleChange = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(user);
    axios
      .post("/api/users", user)
      .then((res) => {
        console.log(res);
        window.location.reload();
        setUser(res.data.data);
      })
      .catch((err) => console.log(err));

    setUser({
      username: "",
      name: "",
      phone: "",
      email: "",
      password: "",
      adress: "",
      dateOfBirth: "",
      status: 1,
      roleId: 2,
      bio: "",
    });
  };

  const {
    username,
    name,
    phone,
    email,
    password,
    adress,
    dateOfBirth,
    status,
    roleId,
    bio,
  } = user;

  return (
    <div className="max-w-[900px] mx-auto mt-20 bg-black text-black p-12 shadow-md rounded-md">
      <h1 className="text-center text-xl my-5">ADD USER</h1>
      <form onSubmit={handleSubmit}>
        <div className="grid grid-cols-2 gap-10">
          <div className="flex flex-col gap-3">
            <div className="flex flex-col gap-3">
              <label className="text-white">Username</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="username"
                value={username}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="text-white">Name</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="name"
                value={name}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="text-white">Phone</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="phone"
                value={phone}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="text-white">Email</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="email"
                value={email}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="text-white">Password</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="password"
                value={password}
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="flex flex-col gap-3">
            <div className="flex flex-col gap-3">
              <label className="text-white">Adress</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="adress"
                value={adress}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="text-white">DateOfBirth</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="dateOfBirth"
                value={dateOfBirth}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="text-white">Status</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="status"
                value={status}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="text-white">RoleId</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="roleId"
                value={roleId}
                onChange={handleChange}
              />
            </div>
            <div className="flex flex-col gap-3">
              <label className="text-white">Bio</label>
              <input
                type="text"
                className="p-2 rounded-sm outline outline-1 focus:outline-offset-1 focus:ring-2"
                name="bio"
                value={bio}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>

        <div className="mt-7 ">
          <button
            className="px-10 w-full py-3 rounded-md bg-sky-400 text-white"
            type="submit"
          >
            submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddUser;
