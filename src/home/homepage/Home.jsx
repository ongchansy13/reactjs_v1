import React, { useState } from 'react'
import SideBar from '../sideBar/SideBar'
import Content from '../content/Content'

const Home = () => {

  const [sidebarOpen, setSidebarOpen] = useState(true);

  const toggleSidebar = () => {
    setSidebarOpen(!sidebarOpen);
  }

  return (
    <div>
      <div className="wrapper">
        <div>
          <div className={`grid ${sidebarOpen ? 'grid-cols-[30%,70%] md:grid-cols-[10%,90%]' : 'grid-cols-1'}`}>

            <SideBar open={sidebarOpen} />

            <Content toggleSidebar={toggleSidebar} />

          </div>
        </div>
      </div>
    </div>
  )
}

export default Home