import React from 'react'
import { Link } from 'react-router-dom'

const SideBar = () => {
  return (
    <>
      <div className="bg-gray-500 h-[200vh] grid justify-center pt-5 ">
        <div >
          <h1 className='text-xl text-white font-sans font-medium mb-5 cursor-pointer px-5'>
            <Link to='/' >DASHBOARD</Link>
          </h1>
          <ul className='flex flex-col gap-5 cursor-pointer px-5'>
            <li className='text-white font-normal text-md'>
              <Link to='/profile'>PROFILE</Link>
            </li>
            <li className='text-white font-normal text-md'>
              <Link to="/add">ADD POST</Link>
            </li>
            <li className='text-white font-normal text-md'>
              <Link to='/shop'>SHOP</Link>
            </li>
            <li className='text-white font-normal text-md'>
              <Link to='/user'>User List</Link>
            </li>
            <li className='text-white font-normal text-md'>
              <Link to='/roll'>User Roll List</Link>
            </li>
            <li className='text-white font-normal text-md'>
              <Link to='/adduser'>Add User</Link>
            </li>
            <li className='text-white font-normal text-md'>
              <Link to='/addroll'>Add User Roll</Link>
            </li>
            <li className='text-white font-normal text-md'>
              <button onClick={() => { localStorage.removeItem('token'); window.location.reload() }}>LOGOUT</button>
            </li>
          </ul>
        </div>
      </div>
    </>
  )
}

export default SideBar