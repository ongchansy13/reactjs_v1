import React from 'react'
import avatar from '../../assets/image/profile4.jpg'
import { Link } from 'react-router-dom'

const Header = ({ toggleSidebar }) => {

  return (
    <div className='bg-slate-50'>

      <div className="nav flex justify-between items-center px-5 py-5">
        <div className="flex items-center gap-4">
          <div onClick={toggleSidebar} className='cursor-pointer'>

            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
              />
            </svg>

          </div>
          <div className='hidden md:block'>
            <input
              type="text"
              placeholder="Search"
              className="w-full rounded-sm px-5 py-2 outline outline-1 outline-gray-300 border-slate-200"
            />
          </div>
        </div>
        <div className="flex items-center gap-3">
          <div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-10 h-10 p-3 bg-gray-200 rounded-full"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M14.857 17.082a23.848 23.848 0 0 0 5.454-1.31A8.967 8.967 0 0 1 18 9.75V9A6 6 0 0 0 6 9v.75a8.967 8.967 0 0 1-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 0 1-5.714 0m5.714 0a3 3 0 1 1-5.714 0M3.124 7.5A8.969 8.969 0 0 1 5.292 3m13.416 0a8.969 8.969 0 0 1 2.168 4.5"
              />
            </svg>
          </div>
          <div>
            <div>
              <Link to='/profile'>

                <img
                  className="rounded-full h-[40px] w-[40px]"
                  src={avatar}
                  alt=""
                />
              </Link>
              <span></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Header