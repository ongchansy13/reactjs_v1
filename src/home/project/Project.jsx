import React from "react";

const email = <svg
  xmlns="http://www.w3.org/2000/svg"
  fill="none"
  viewBox="0 0 24 24"
  strokeWidth={1.5}
  stroke="currentColor"
  className="w-10 h-10 p-2 bg-blue-300 text-blue-700 rounded-md"
>
  <path
    strokeLinecap="round"
    strokeLinejoin="round"
    d="M21.75 6.75v10.5a2.25 2.25 0 0 1-2.25 2.25h-15a2.25 2.25 0 0 1-2.25-2.25V6.75m19.5 0A2.25 2.25 0 0 0 19.5 4.5h-15a2.25 2.25 0 0 0-2.25 2.25m19.5 0v.243a2.25 2.25 0 0 1-1.07 1.916l-7.5 4.615a2.25 2.25 0 0 1-2.36 0L3.32 8.91a2.25 2.25 0 0 1-1.07-1.916V6.75"
  />
</svg>;

const list = <svg
  xmlns="http://www.w3.org/2000/svg"
  fill="none"
  viewBox="0 0 24 24"
  strokeWidth={1.5}
  stroke="currentColor"
  className="w-10 h-10 p-2 bg-blue-300 text-blue-700 rounded-md"
>
  <path
    strokeLinecap="round"
    strokeLinejoin="round"
    d="M8.25 6.75h12M8.25 12h12m-12 5.25h12M3.75 6.75h.007v.008H3.75V6.75Zm.375 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0ZM3.75 12h.007v.008H3.75V12Zm.375 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Zm-.375 5.25h.007v.008H3.75v-.008Zm.375 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Z"
  />
</svg>;

const people = <svg
  xmlns="http://www.w3.org/2000/svg"
  fill="none"
  viewBox="0 0 24 24"
  strokeWidth={1.5}
  stroke="currentColor"
  className="w-10 h-10 p-2 bg-blue-300 text-blue-700 rounded-md"
>
  <path
    strokeLinecap="round"
    strokeLinejoin="round"
    d="M18 18.72a9.094 9.094 0 0 0 3.741-.479 3 3 0 0 0-4.682-2.72m.94 3.198.001.031c0 .225-.012.447-.037.666A11.944 11.944 0 0 1 12 21c-2.17 0-4.207-.576-5.963-1.584A6.062 6.062 0 0 1 6 18.719m12 0a5.971 5.971 0 0 0-.941-3.197m0 0A5.995 5.995 0 0 0 12 12.75a5.995 5.995 0 0 0-5.058 2.772m0 0a3 3 0 0 0-4.681 2.72 8.986 8.986 0 0 0 3.74.477m.94-3.197a5.971 5.971 0 0 0-.94 3.197M15 6.75a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm6 3a2.25 2.25 0 1 1-4.5 0 2.25 2.25 0 0 1 4.5 0Zm-13.5 0a2.25 2.25 0 1 1-4.5 0 2.25 2.25 0 0 1 4.5 0Z"
  />
</svg>;

const notePen = <svg
  xmlns="http://www.w3.org/2000/svg"
  fill="none"
  viewBox="0 0 24 24"
  strokeWidth={1.5}
  stroke="currentColor"
  className="w-10 h-10 p-2 bg-blue-300 text-blue-700 rounded-md"
>
  <path
    strokeLinecap="round"
    strokeLinejoin="round"
    d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10"
  />
</svg>;

const projectActivityData = [
  {
    name: "project",
    icon: email,
    number: '18',
    task: '2 Complete',
  },
  {
    name: "Active Task",
    icon: list,
    number: '32',
    task: '28 Complete',
  },
  {
    name: "Teams",
    icon: people,
    number: '12',
    task: '1 Complete',
  },
  {
    name: "Productivity",
    icon: notePen,
    number: '76 %',
    task: '5% Complete',
  },
];



const Project = () => {
  return (
    <div>
      <div className="bg-blue-600 text-white w-full h-[200px] py-5"></div>

      <div className="px-10 -mt-40 md:-mt-32">
        <div className="flex flex-col gap-5 md:gap-0 md:flex-row justify-between items-center mb-5">
          <h1 className="font-medium text-2xl justify-self-center text-white">
            Project
          </h1>
          <button className="bg-white shadow-md text-gray-500 px-4 py-2 rounded-md font-medium">
            Create New Project
          </button>
        </div>

        <div className="grid md:grid-cols-4 gap-5">

          {
            projectActivityData.map((project, index) => {
              return (
                <div key={index} className="bg-white rounded-md shadow-md text-gray-700">
                  <div className="p-5 ">
                    <div className="flex justify-between items-center ">
                      <p className="text-2xl mb-5">{project.name}</p>
                      <div>
                        {project.icon}
                      </div>
                    </div>

                    <div>
                      <p className="font-bold text-4xl mb-2">{project.number}</p>
                    </div>
                    <div>
                      <p className="text-lg text-gray-500">{project.task}</p>
                    </div>
                  </div>
                </div>
              )
            })
          }
        </div>
      </div>
    </div >
  );
};

export default Project;
