import React from 'react'

import profile1 from '../../assets/image/profile1.jpg'
import profile2 from '../../assets/image/profile2.jpg'
import profile4 from '../../assets/image/profile4.jpg'


const ProjectTable = () => {
  return (
    <div>
      <div className="mt-5 w-full px-10">
        <div className="bg-white py-5 shadow-lg rounded-t-md">
          <h1 className="font-medium text-lg pl-5">Active Projects</h1>
        </div>
      </div>

      <div className="px-10 overflow-x-auto">

        <table className="table-auto w-full bg-white  ">
          <caption className="caption-bottom border-b-2 rounded-b-md">
            <p className="text-blue-300 cursor-pointer py-3">View All Projects</p>
          </caption>
          <thead className="bg-slate-200 border-b border-t border-gray-300 ">
            <tr className="">
              <th className="py-4 text-left px-5 text-gray-500">
                Project Name
              </th>
              <th className="py-4 text-left px-5 text-gray-500">Hours</th>
              <th className="py-4 text-left px-5 text-gray-500">
                Priority
              </th>
              <th className="py-3 text-left px-5 text-gray-500">
                Members
              </th>
              <th className="py-4 text-left px-5 text-gray-500">
                Progress
              </th>
            </tr>
          </thead>

          <tbody>
            <tr className="border-b bg-white border-gray-300 border-t">
              <td className="px-5 py-4">
                <div className="flex items-center gap-5">
                  <div className="border bg-white">
                    <div className="p-2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 23"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M5.99998 0.962402L0 4.81144L5.99998 8.60388L12 4.81144L5.99998 0.962402ZM17.9999 0.962402L12 4.81144L17.9999 8.60388L23.9999 4.81144L17.9999 0.962402ZM0 12.4529L5.99998 16.302L12 12.4529L5.99998 8.60388L0 12.4529ZM17.9999 8.60388L12 12.4529L17.9999 16.302L23.9999 12.4529L17.9999 8.60388ZM5.99998 17.5472L12 21.3963L17.9999 17.5472L12 13.7548L5.99998 17.5472Z"
                          fill="#0062FF"
                        />
                      </svg>
                    </div>
                  </div>
                  <p className="text-lg">Dropbox Design System</p>
                </div>
              </td>
              <td className="px-5 py-4">
                <p>75</p>
              </td>
              <td className="px-5 py-4">
                <span className="bg-red-500 px-3 text-sm py-1 rounded-full text-white">
                  Height
                </span>
              </td>
              <td className="px-5 py-4">
                <div>
                  <div className="flex items-center last:mr-0">
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-white  rounded-full"
                      src={profile1}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-white rounded-full"
                      src={profile2}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-white  rounded-full"
                      src={profile4}
                      alt=""
                    />
                    <div className="h-10 w-10 mr-[-28px] border-[2px] border-white bg-blue-500 rounded-full text-white flex items-center justify-center">
                      <p>+5</p>
                    </div>
                  </div>
                </div>
              </td>
              <td className="px-5 py-4">
                <div className="flex items-center gap-2">
                  <p>75%</p>
                  <div>
                    <div className="h-2 bg-slate-300 rounded-full w-[100px]">
                      <div className="h-2 bg-blue-500 rounded-full w-[75px]" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr className="border-b bg-white border-gray-300 border-t">
              <td className="px-5 py-4">
                <div className="flex items-center gap-5">
                  <div className="border bg-white">
                    <div className="p-2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clip-path="url(#clip0)">
                          <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M8.78141 0C7.45737 0.000978593 6.38581 1.07547 6.38679 2.39951C6.38581 3.72355 7.45835 4.79804 8.78239 4.79902H11.178V2.40049C11.179 1.07645 10.1064 0.00195719 8.78141 0C8.78239 0 8.78239 0 8.78141 0V0ZM8.78141 6.4H2.39511C1.07107 6.40098 -0.0014642 7.47547 -0.000485607 8.79951C-0.00244279 10.1235 1.0701 11.198 2.39413 11.2H8.78141C10.1054 11.199 11.178 10.1245 11.177 8.80049C11.178 7.47547 10.1054 6.40098 8.78141 6.4Z"
                            fill="#36C5F0"
                          />
                          <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M23.952 8.79951C23.953 7.47547 22.8805 6.40098 21.5564 6.4C20.2324 6.40098 19.1598 7.47547 19.1608 8.79951V11.2H21.5564C22.8805 11.199 23.953 10.1245 23.952 8.79951ZM17.5647 8.79951V2.39951C17.5657 1.07645 16.4941 0.00195719 15.17 0C13.846 0.000978593 12.7734 1.07547 12.7744 2.39951V8.79951C12.7725 10.1235 13.845 11.198 15.1691 11.2C16.4931 11.199 17.5657 10.1245 17.5647 8.79951Z"
                            fill="#2EB67D"
                          />
                          <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M15.1691 24.0002C16.4931 23.9992 17.5657 22.9247 17.5647 21.6006C17.5657 20.2766 16.4931 19.2021 15.1691 19.2011H12.7734V21.6006C12.7725 22.9237 13.845 23.9982 15.1691 24.0002ZM15.1691 17.5991H21.5564C22.8805 17.5981 23.953 16.5236 23.952 15.1996C23.954 13.8755 22.8814 12.801 21.5574 12.7991H15.17C13.846 12.8001 12.7734 13.8746 12.7744 15.1986C12.7734 16.5236 13.845 17.5981 15.1691 17.5991Z"
                            fill="#ECB22E"
                          />
                          <path
                            fill-rule="evenodd"
                            clip-rule="evenodd"
                            d="M6.69125e-07 15.1993C-0.000977924 16.5234 1.07156 17.5979 2.3956 17.5989C3.71963 17.5979 4.79217 16.5234 4.79119 15.1993V12.7998H2.3956C1.07156 12.8008 -0.000977924 13.8753 6.69125e-07 15.1993ZM6.38728 15.1993V21.5994C6.38532 22.9235 7.45786 23.998 8.7819 23.9999C10.1059 23.9989 11.1785 22.9244 11.1775 21.6004V15.2013C11.1795 13.8772 10.1069 12.8027 8.78288 12.8008C7.45786 12.8008 6.3863 13.8753 6.38728 15.1993C6.38728 15.1993 6.38728 15.2003 6.38728 15.1993Z"
                            fill="#E01E5A"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0">
                            <rect
                              width="23.952"
                              height="24"
                              fill="white"
                            />
                          </clipPath>
                        </defs>
                      </svg>
                    </div>
                  </div>
                  <p className="text-lg">Slack Team UI Design</p>
                </div>
              </td>
              <td className="px-5 py-4">
                <p>34</p>
              </td>
              <td className="px-5 py-4">
                <span className="bg-yellow-600 px-3 text-sm py-1 rounded-full text-white">
                  Medium
                </span>
              </td>
              <td className="px-5 py-4">
                <div>
                  <div className="flex items-center last:mr-0">
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile1}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile2}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile4}
                      alt=""
                    />
                    <div className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500 bg-blue-500 rounded-full text-white flex items-center justify-center">
                      <p>+5</p>
                    </div>
                  </div>
                </div>
              </td>
              <td className="px-5 py-4">
                <div className="flex items-center gap-2">
                  <p>34%</p>
                  <div>
                    <div className="h-2 bg-slate-300 rounded-full w-[100px]">
                      <div className="h-2 bg-blue-500 rounded-full w-[34px]" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr className="border-b bg-white border-gray-300 border-t">
              <td className="px-5 py-4">
                <div className="flex items-center gap-5">
                  <div className="border bg-white">
                    <div className="p-2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clip-path="url(#clip0)">
                          <path
                            d="M11.9536 0C5.35266 0 0 5.35172 0 11.9536C0 17.2351 3.42506 21.7158 8.17462 23.2964C8.772 23.407 8.99137 23.0371 8.99137 22.7213C8.99137 22.4363 8.98022 21.4946 8.97516 20.4958C5.64956 21.2189 4.94784 19.0854 4.94784 19.0854C4.40409 17.7037 3.62062 17.3364 3.62062 17.3364C2.53612 16.5945 3.70237 16.6097 3.70237 16.6097C4.90275 16.6941 5.53481 17.8416 5.53481 17.8416C6.60094 19.669 8.33119 19.1407 9.01331 18.8353C9.12056 18.0626 9.43041 17.5354 9.77222 17.2369C7.11722 16.9345 4.32609 15.9096 4.32609 11.3293C4.32609 10.0243 4.79306 8.9579 5.55778 8.12081C5.43366 7.81969 5.02453 6.60394 5.67356 4.95741C5.67356 4.95741 6.67734 4.63612 8.96166 6.18272C9.91509 5.91778 10.9377 5.78503 11.9536 5.78053C12.9695 5.78503 13.9928 5.91778 14.9482 6.18272C17.2297 4.63612 18.2321 4.95741 18.2321 4.95741C18.8827 6.60394 18.4734 7.81969 18.3493 8.12081C19.1157 8.9579 19.5795 10.0242 19.5795 11.3293C19.5795 15.9204 16.7831 16.9314 14.1214 17.2273C14.5501 17.5983 14.9321 18.3258 14.9321 19.4409C14.9321 21.0403 14.9182 22.3276 14.9182 22.7213C14.9182 23.0394 15.1334 23.4122 15.7394 23.2948C20.4863 21.7124 23.9071 17.2333 23.9071 11.9536C23.9071 5.35172 18.5552 0 11.9536 0Z"
                            fill="#161614"
                          />
                          <path
                            d="M4.4769 17.0281C4.45065 17.0875 4.35709 17.1053 4.27205 17.0646C4.18534 17.0256 4.13659 16.9446 4.16471 16.885C4.19049 16.8239 4.28405 16.8068 4.37059 16.8479C4.45749 16.8868 4.50699 16.9685 4.4769 17.0281ZM5.06489 17.5528C5.00789 17.6056 4.89643 17.5811 4.82077 17.4975C4.74258 17.4142 4.72796 17.3028 4.7858 17.2491C4.84458 17.1963 4.95267 17.221 5.03105 17.3044C5.10924 17.3887 5.12442 17.4994 5.0648 17.5529L5.06489 17.5528ZM5.4683 18.224C5.39498 18.2749 5.27517 18.2272 5.2012 18.1209C5.12799 18.0147 5.12799 17.8872 5.2028 17.8361C5.27705 17.785 5.39498 17.8309 5.46998 17.9364C5.54311 18.0445 5.54311 18.172 5.4682 18.2241L5.4683 18.224ZM6.15042 19.0015C6.08489 19.0737 5.94539 19.0544 5.8432 18.9557C5.73876 18.8594 5.70961 18.7226 5.77532 18.6503C5.84161 18.5779 5.98195 18.5983 6.08488 18.696C6.18867 18.7922 6.22035 18.93 6.15051 19.0015H6.15042ZM7.03203 19.264C7.00325 19.3575 6.86882 19.4001 6.73344 19.3604C6.59826 19.3194 6.50976 19.2097 6.53704 19.1151C6.56516 19.0209 6.70016 18.9765 6.83657 19.0191C6.97157 19.0599 7.06025 19.1687 7.03213 19.264H7.03203ZM8.03553 19.3753C8.0389 19.4739 7.92406 19.5556 7.78193 19.5574C7.63897 19.5605 7.52337 19.4807 7.52187 19.3838C7.52187 19.2842 7.63409 19.2032 7.77697 19.2009C7.91909 19.1981 8.03553 19.2773 8.03553 19.3753ZM9.02121 19.3375C9.03827 19.4337 8.93946 19.5325 8.79836 19.5587C8.65962 19.584 8.53118 19.5247 8.51346 19.4294C8.49621 19.3307 8.5969 19.232 8.73536 19.2064C8.87674 19.1819 9.00321 19.2397 9.02121 19.3375Z"
                            fill="#161614"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0">
                            <rect
                              width="24"
                              height="23.3472"
                              fill="white"
                            />
                          </clipPath>
                        </defs>
                      </svg>
                    </div>
                  </div>
                  <p className="text-lg">GitHub Satellite</p>
                </div>
              </td>
              <td className="px-5 py-4">
                <p>91</p>
              </td>
              <td className="px-5 py-4">
                <span className="bg-red-500 px-3 text-sm py-1 rounded-full text-white">
                  Height
                </span>
              </td>
              <td className="px-5 py-4">
                <div>
                  <div className="flex items-center last:mr-0">
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile1}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile2}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile4}
                      alt=""
                    />
                    <div className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500 bg-blue-500 rounded-full text-white flex items-center justify-center">
                      <p>+5</p>
                    </div>
                  </div>
                </div>
              </td>
              <td className="px-5 py-4">
                <div className="flex items-center gap-2">
                  <p>91%</p>
                  <div>
                    <div className="h-2 bg-slate-300 rounded-full w-[100px]">
                      <div className="h-2 bg-blue-500 rounded-full w-[91px]" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr className="border-b bg-white border-gray-300 border-t">
              <td className="px-5 py-4">
                <div className="flex items-center gap-5">
                  <div className="border bg-white">
                    <div className="p-2">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clip-path="url(#clip0)">
                          <path
                            opacity="0.75"
                            d="M13.8288 11.5919L14.5183 9.39216L10.8173 10.9285L13.8288 11.5919ZM21.8593 7.43687C19.0399 4.59123 12.2052 5.12369 9.51669 5.78709C6.37429 6.59016 5.53632 8.9819 5.62361 12.8576C5.65852 15.0834 6.62743 17.0562 8.50414 17.7283C12.3274 19.0551 15.6094 18.8718 19.0574 15.5723C17.2941 13.6693 15.8277 13.0059 13.4185 13.1805C11.0792 13.294 9.75237 12.9274 8.09388 11.1118C10.119 9.1652 11.3585 8.22248 13.8026 7.96934C16.5347 7.69001 18.9701 8.84223 20.0263 9.91589C21.0126 10.9285 21.5102 13.2416 21.2745 16.0436C21.0912 18.3132 20.3667 19.6487 18.6995 20.1288C16.5522 20.7922 13.436 21.1152 10.5554 21.0017C7.15989 20.862 5.3792 20.1724 4.88165 19.6487C3.83419 18.479 3.06605 16 2.89147 13.1543C2.71689 10.3087 3.16207 7.62018 4.04368 6.15371C4.91657 4.68724 8.14625 3.6485 12.9035 3.35171C16.8751 3.12476 20.5587 3.42154 20.6024 3.42154L20.358 0.25293C20.358 0.25293 11.8648 1.2393 10.5642 1.35278C7.41303 1.65829 3.17079 2.33915 1.74799 4.73089C-0.792115 8.99935 -0.146178 18.0862 2.92639 21.4905C5.66725 24.5456 15.1992 24.0394 19.4676 22.7562C21.388 22.1801 23.5615 20.6001 23.9368 16.2793C24.1812 13.4337 23.7884 9.40961 21.8593 7.43687ZM13.3225 15.0573L16.3339 15.7207L12.6329 17.257L13.3225 15.0573Z"
                            fill="black"
                          />
                          <path
                            d="M13.6539 11.3388L14.3435 9.13913L10.6425 10.6754L13.6539 11.3388ZM21.6845 7.18386C18.8651 4.33825 12.0304 4.86198 9.34188 5.53411C6.19949 6.33716 5.36152 8.72887 5.4488 12.6045C5.48372 14.8304 6.45263 16.8031 8.32933 17.4752C12.1526 18.802 15.4346 18.6187 18.8825 15.3192C17.1193 13.4163 15.6529 12.7529 13.2437 12.9275C10.9044 13.0409 9.57756 12.6743 7.91908 10.8587C9.94418 8.91218 11.1837 7.96946 13.6278 7.71633C16.3599 7.437 18.7953 8.58921 19.8514 9.66286C20.8378 10.6754 21.3354 12.9886 21.0997 15.7905C20.9164 18.06 20.1919 19.3956 18.5247 19.8756C16.3774 20.539 13.2524 20.862 10.3806 20.7485C6.98509 20.6089 5.2044 19.9193 4.70685 19.3956C3.65938 18.2259 2.89124 15.7469 2.71667 12.9013C2.54209 10.0557 2.98726 7.36717 3.86888 5.90072C4.74177 4.43427 7.97145 3.39553 12.7287 3.09875C16.7003 2.8718 20.3839 3.16858 20.4276 3.16858L20.1831 0C20.1831 0 11.69 0.986363 10.3893 1.09984C7.23822 1.39662 2.98726 2.0862 1.57318 4.47791C-0.966919 8.74633 -0.320983 17.8418 2.75158 21.2374C5.49245 24.2925 15.0244 23.7862 19.2928 22.503C21.2132 21.9269 23.3866 20.347 23.762 16.0262C24.0064 13.1806 23.6136 9.15659 21.6845 7.18386ZM13.1477 14.8042L16.1591 15.4676L12.4581 17.0038L13.1477 14.8042Z"
                            fill="#016B6B"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0">
                            <rect
                              width="24"
                              height="23.76"
                              fill="white"
                            />
                          </clipPath>
                        </defs>
                      </svg>
                    </div>
                  </div>
                  <p className="text-lg">3D Character Modelling</p>
                </div>
              </td>
              <td className="px-5 py-4">
                <p>120</p>
              </td>
              <td className="px-5 py-4">
                <span className="bg-sky-300 px-3 text-sm py-1 rounded-full text-white">
                  Low
                </span>
              </td>
              <td className="px-5 py-4">
                <div>
                  <div className="flex items-center last:mr-0">
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile1}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile2}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile4}
                      alt=""
                    />
                    <div className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500 bg-blue-500 rounded-full text-white flex items-center justify-center">
                      <p>+5</p>
                    </div>
                  </div>
                </div>
              </td>
              <td className="px-5 py-4">
                <div className="flex items-center gap-2">
                  <p>100%</p>
                  <div>
                    <div className="h-2 bg-slate-300 rounded-full w-[100px]">
                      <div className="h-2 bg-blue-500 rounded-full w-[100pxpx]" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr className="border-b bg-white border-gray-300 border-t">
              <td className="px-5 py-4">
                <div className="flex items-center gap-5">
                  <div className="border bg-white">
                    <div className="p-2">
                      <svg
                        width="24px"
                        height="24px"
                        viewBox="0 0 1024 1024"
                        className="icon"
                        version="1.1"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M512 960c-92.8 0-160-200-160-448S419.2 64 512 64s160 200 160 448-67.2 448-160 448z m0-32c65.6 0 128-185.6 128-416S577.6 96 512 96s-128 185.6-128 416 62.4 416 128 416z"
                          fill="#050D42"
                        />
                        <path
                          d="M124.8 736c-48-80 92.8-238.4 307.2-363.2S852.8 208 899.2 288 806.4 526.4 592 651.2 171.2 816 124.8 736z m27.2-16c33.6 57.6 225.6 17.6 424-97.6S905.6 361.6 872 304 646.4 286.4 448 401.6 118.4 662.4 152 720z"
                          fill="#050D42"
                        />
                        <path
                          d="M899.2 736c-46.4 80-254.4 38.4-467.2-84.8S76.8 368 124.8 288s254.4-38.4 467.2 84.8S947.2 656 899.2 736z m-27.2-16c33.6-57.6-97.6-203.2-296-318.4S184 246.4 152 304 249.6 507.2 448 622.4s392 155.2 424 97.6z"
                          fill="#050D42"
                        />
                        <path
                          d="M512 592c-44.8 0-80-35.2-80-80s35.2-80 80-80 80 35.2 80 80-35.2 80-80 80zM272 312c-27.2 0-48-20.8-48-48s20.8-48 48-48 48 20.8 48 48-20.8 48-48 48zM416 880c-27.2 0-48-20.8-48-48s20.8-48 48-48 48 20.8 48 48-20.8 48-48 48z m448-432c-27.2 0-48-20.8-48-48s20.8-48 48-48 48 20.8 48 48-20.8 48-48 48z"
                          fill="#2F4BFF"
                        />
                      </svg>
                    </div>
                  </div>
                  <p className="text-lg">Webapp Design System</p>
                </div>
              </td>
              <td className="px-5 py-4">
                <p>108</p>
              </td>
              <td className="px-5 py-4">
                <span className="bg-green-800 px-3 text-sm py-1 rounded-full text-white">
                  Tract
                </span>
              </td>
              <td className="px-5 py-4">
                <div>
                  <div className="flex items-center last:mr-0">
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile1}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile2}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile4}
                      alt=""
                    />
                    <div className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500 bg-blue-500 rounded-full text-white flex items-center justify-center">
                      <p>+5</p>
                    </div>
                  </div>
                </div>
              </td>
              <td className="px-5 py-4">
                <div className="flex items-center gap-2">
                  <p>108%</p>
                  <div>
                    <div className="h-2 bg-slate-300 rounded-full w-[100px]">
                      <div className="h-2 bg-blue-500 rounded-full w-[100px]" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>
            <tr className="border-b bg-white border-gray-300 border-t">
              <td className="px-5 py-4">
                <div className="flex items-center gap-5">
                  <div className="border bg-white">
                    <div className="p-2">
                      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0)">
                          <path d="M11.9536 0C5.35266 0 0 5.35172 0 11.9536C0 17.2351 3.42506 21.7158 8.17462 23.2964C8.772 23.407 8.99137 23.0371 8.99137 22.7213C8.99137 22.4363 8.98022 21.4946 8.97516 20.4958C5.64956 21.2189 4.94784 19.0854 4.94784 19.0854C4.40409 17.7037 3.62062 17.3364 3.62062 17.3364C2.53612 16.5945 3.70237 16.6097 3.70237 16.6097C4.90275 16.6941 5.53481 17.8416 5.53481 17.8416C6.60094 19.669 8.33119 19.1407 9.01331 18.8353C9.12056 18.0626 9.43041 17.5354 9.77222 17.2369C7.11722 16.9345 4.32609 15.9096 4.32609 11.3293C4.32609 10.0243 4.79306 8.9579 5.55778 8.12081C5.43366 7.81969 5.02453 6.60394 5.67356 4.95741C5.67356 4.95741 6.67734 4.63612 8.96166 6.18272C9.91509 5.91778 10.9377 5.78503 11.9536 5.78053C12.9695 5.78503 13.9928 5.91778 14.9482 6.18272C17.2297 4.63612 18.2321 4.95741 18.2321 4.95741C18.8827 6.60394 18.4734 7.81969 18.3493 8.12081C19.1157 8.9579 19.5795 10.0242 19.5795 11.3293C19.5795 15.9204 16.7831 16.9314 14.1214 17.2273C14.5501 17.5983 14.9321 18.3258 14.9321 19.4409C14.9321 21.0403 14.9182 22.3276 14.9182 22.7213C14.9182 23.0394 15.1334 23.4122 15.7394 23.2948C20.4863 21.7124 23.9071 17.2333 23.9071 11.9536C23.9071 5.35172 18.5552 0 11.9536 0Z" fill="#161614" />
                          <path d="M4.4769 17.0281C4.45065 17.0875 4.35709 17.1053 4.27205 17.0646C4.18534 17.0256 4.13659 16.9446 4.16471 16.885C4.19049 16.8239 4.28405 16.8068 4.37059 16.8479C4.45749 16.8868 4.50699 16.9685 4.4769 17.0281ZM5.06489 17.5528C5.00789 17.6056 4.89643 17.5811 4.82077 17.4975C4.74258 17.4142 4.72796 17.3028 4.7858 17.2491C4.84458 17.1963 4.95267 17.221 5.03105 17.3044C5.10924 17.3887 5.12442 17.4994 5.0648 17.5529L5.06489 17.5528ZM5.4683 18.224C5.39498 18.2749 5.27517 18.2272 5.2012 18.1209C5.12799 18.0147 5.12799 17.8872 5.2028 17.8361C5.27705 17.785 5.39498 17.8309 5.46998 17.9364C5.54311 18.0445 5.54311 18.172 5.4682 18.2241L5.4683 18.224ZM6.15042 19.0015C6.08489 19.0737 5.94539 19.0544 5.8432 18.9557C5.73876 18.8594 5.70961 18.7226 5.77532 18.6503C5.84161 18.5779 5.98195 18.5983 6.08488 18.696C6.18867 18.7922 6.22035 18.93 6.15051 19.0015H6.15042ZM7.03203 19.264C7.00325 19.3575 6.86882 19.4001 6.73344 19.3604C6.59826 19.3194 6.50976 19.2097 6.53704 19.1151C6.56516 19.0209 6.70016 18.9765 6.83657 19.0191C6.97157 19.0599 7.06025 19.1687 7.03213 19.264H7.03203ZM8.03553 19.3753C8.0389 19.4739 7.92406 19.5556 7.78193 19.5574C7.63897 19.5605 7.52337 19.4807 7.52187 19.3838C7.52187 19.2842 7.63409 19.2032 7.77697 19.2009C7.91909 19.1981 8.03553 19.2773 8.03553 19.3753ZM9.02121 19.3375C9.03827 19.4337 8.93946 19.5325 8.79836 19.5587C8.65962 19.584 8.53118 19.5247 8.51346 19.4294C8.49621 19.3307 8.5969 19.232 8.73536 19.2064C8.87674 19.1819 9.00321 19.2397 9.02121 19.3375Z" fill="#161614" />
                        </g>
                        <defs>
                          <clipPath id="clip0">
                            <rect width="24" height="23.3472" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                    </div>
                  </div>
                  <p className="text-lg">Github Event Design</p>
                </div>
              </td>
              <td className="px-5 py-4">
                <p>108</p>
              </td>
              <td className="px-5 py-4">
                <span className="bg-sky-300 px-3 text-sm py-1 rounded-full text-white">
                  Tract
                </span>
              </td>
              <td className="px-5 py-4">
                <div>
                  <div className="flex items-center last:mr-0">
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile1}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile2}
                      alt=""
                    />
                    <img
                      className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500  rounded-full"
                      src={profile4}
                      alt=""
                    />
                    <div className="h-10 w-10 mr-[-28px] border-[2px] border-blue-500 bg-blue-500 rounded-full text-white flex items-center justify-center">
                      <p>+5</p>
                    </div>
                  </div>
                </div>
              </td>
              <td className="px-5 py-4">
                <div className="flex items-center gap-2">
                  <p>57%</p>
                  <div>
                    <div className="h-2 bg-slate-300 rounded-full w-[100px]">
                      <div className="h-2 bg-blue-500 rounded-full w-[57px]" />
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default ProjectTable