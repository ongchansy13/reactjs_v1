import React, { Component } from 'react'

export default class extends Component {

  componentWillUnmount() {
    alert('the component is to be unmounted');
  }

  render() {
    return (
      <div>
        <h1>Hello World</h1>
        <p>Hi, There</p>
        <p>hello, i am me.</p>

      </div>
    )
  }
}
