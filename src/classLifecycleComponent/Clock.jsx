import React, { Component } from 'react'

export default class Clock extends Component {

  constructor(props) {
    super(props)

    this.state = {
      favoriteColor: 'red'
    }
  }

  // static getDerivedStateFromProps(props, state) {
  //   return {
  //     favoriteColor: props.favol,
  //   }
  // }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        favoriteColor: 'blue'
      })
    }, 2000)
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    document.getElementById('div1').innerHTML = "Before the update, Favorite color is : " + prevState.favoriteColor;
  }

  componentDidUpdate() {
    document.getElementById('div2').innerHTML = "After the update, Favorite color is : " + this.state.favoriteColor;
  }



  render() {
    return (

      <div>
        <div>
          <p>{this.state.favoriteColor}</p>
        </div>

        <div id='div1'></div>
        <div id='div2'></div>

        {/* <button onClick={this.handleChange}>click</button> */}
      </div>
    )
  }
}

