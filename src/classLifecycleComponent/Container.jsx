import React, { Component } from 'react'
import Child from './Child'

export default class Container extends Component {

  constructor(props) {
    super(props)
    this.state = {
      show: true,
    }
  }

  delete = () => {
    this.setState({
      show: false,
    })
  }

  render() {

    let Myheader;
    if (this.state.show) {
      Myheader = <Child />
    }
    return (
      <div>
        {Myheader}
        <button onClick={this.delete}>Delete</button>
        <h1>haha</h1>
      </div>
    )
  }
}
