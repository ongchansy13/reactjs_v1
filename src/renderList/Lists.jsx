import React from 'react'
import ListItems from './ListItems'

const Lists = () => {

  const cars = [
    {
      id: 1,
      name: 'toyota'
    },
    {
      id: 2,
      name: 'yamaha'
    },
    {
      id: 3,
      name: 'mazda'
    },
  ]

  return (
    <div>
      {
        cars.map((c) => {
          return (

            <ListItems key={c.id} brand={c.name} />
          )
        })
      }
    </div>
  )
}

export default Lists