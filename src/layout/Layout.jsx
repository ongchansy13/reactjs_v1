import React, { useState } from 'react'
import { Outlet } from 'react-router-dom'
import Header from '../home/header/Header'
import SideBar from '../home/sideBar/SideBar';

const Layout = () => {

  const [sidebarOpen, setSidebarOpen] = useState(true);

  const toggleSidebar = () => {
    setSidebarOpen(!sidebarOpen);
  }

  return (
    <div>
      <div className={`grid  ${sidebarOpen ? 'grid-cols-[30%,70%] md:grid-cols-[10%,90%]' : 'grid-cols-1'}  `}>
        <div className={` ${sidebarOpen ? 'block' : 'hidden'}`}>
          <SideBar />
        </div>

        <div className=''>
          <div className=''>
            <Header toggleSidebar={toggleSidebar} />
          </div>
          <main className='px-5'>
            <Outlet />
          </main>
        </div>
      </div>
    </div>
  )
}

export default Layout