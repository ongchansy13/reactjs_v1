import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./layout/Layout";
import HomePage from "./home/HomePage";
import Contact from "./Contact/Contact";
import Profile from "./home/components/feature/Profile";
import Category from "./home/components/feature/Category";
import AboutMe from "./home/components/feature/AboutMe";
import AddProject from "./home/components/feature/AddProject";
import Blog from "./home/components/feature/Blog";
import Shop from "./home/components/shopingCart/Shop";
import FormLogin from "./login/FormLogin";
import GetUser from "./home/components/getUser/GetUser";
import UserRole from "./home/components/Role/UserRole";
import AddUser from "./home/components/add/addUser/AddUser";
import AddRoll from "./home/components/add/addRoll/AddRoll";
import EditUser from "./home/components/delete/editUser/EditUser";
import ViewUser from "./home/components/view/ViewUser";

const App = () => {
  const Auth = localStorage.getItem("token");
  return (
    <>
      <BrowserRouter>
        <Routes>
          {Auth ? (
            <Route path="/" element={<Layout />}>
              <Route index element={<HomePage />} />
              <Route path="profile" element={<Profile />} />
              <Route path="contact" element={<Contact />} />
              <Route path="category" element={<Category />} />
              <Route path="about" element={<AboutMe />} />
              <Route path="add" element={<AddProject />} />
              <Route path="blog" element={<Blog />} />
              <Route path="shop" element={<Shop />} />
              <Route path="user" element={<GetUser />} />
              <Route path="adduser" element={<AddUser />} />
              <Route path="addroll" element={<AddRoll />} />
              <Route path={`view/:id`} element={<ViewUser />} />
              <Route path={`edit/:id`} element={<EditUser />} />
              <Route path="roll" element={<UserRole />} />
            </Route>
          ) : (
            <Route path="*" element={<FormLogin />} />
          )}
          <Route path="*" element={<div>no page</div>} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
