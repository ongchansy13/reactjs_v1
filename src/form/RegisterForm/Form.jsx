import React, { Component } from "react";

export default class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        fname: "",
        lname: "",
        company: "",
        email: "",
        phone: "",
        subject: null,
        gender: [],
      },
      records: [],
    };
  }


  updateInput = (e) => {
    console.log(e.target.name, e.target.value);
    this.setState((prev) => ({
      ...prev,
      form: {
        ...prev.form,
        [e.target.name]: e.target.value,

      },
    }));
  }

  updateCheckbox = (e) => {

    let checkboxes = [...this.state.form.gender];

    if (e.target.checked) {
      checkboxes.push(e.target.value);
    } else {
      checkboxes = checkboxes.filter((checkbox) => checkbox !== e.target.value);
    }
    this.setState((prev) => ({
      ...prev,
      form: {
        ...prev.form,
        gender: checkboxes,
      },
    }));
    console.log(checkboxes);
  }

  onSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.form);
    this.setState((prev) => ({
      records: [...prev.records, this.state.form],
    }));
    this.setState({ form: '' })
  }






  render() {
    const { fname, lname, company, email, phone } = this.state.form;
    const { records } = this.state;
    return (
      <React.Fragment>
        <div className="">
          <div className="max-w-[900px] mx-auto absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 rounded-lg shadow-lg p-20">
            <div>
              <h1 className="uppercase text-2xl font-semibold mb-10 ">
                Register Form
              </h1>
            </div>

            <form onSubmit={this.onSubmit} >
              <div className="flex flex-col gap-10">
                <div className="flex items-center gap-10">
                  <div className="flex  gap-5">
                    <label>FIRST NAME :</label>
                    <input
                      type="text"
                      name="fname"
                      value={fname}
                      onChange={this.updateInput}
                      className="outline outline-offset-1 outline-1 focus:ring-4"
                    />
                  </div>

                  <div className=" flex  gap-5">
                    <label>LAST NAME :</label>
                    <input
                      type="text"
                      name="lname"
                      value={lname}
                      onChange={this.updateInput}
                      className="outline outline-offset-1 outline-1 focus:ring-4 "
                    />
                  </div>
                </div>

                <div className="flex flex-col gap-5">
                  <div className=" flex  gap-5 ">
                    <label>COMPANY </label>
                    <input
                      type="text"
                      name="company"
                      value={company}
                      onChange={this.updateInput}
                      className="outline outline-offset-1 outline-1 focus:ring-4 w-full "
                    />
                  </div>
                  <div className=" flex  gap-5 ">
                    <label className="mr-7">EMAIL </label>
                    <input
                      type="email"
                      name="email"
                      value={email}
                      onChange={this.updateInput}
                      className="outline outline-offset-1 outline-1 focus:ring-4 w-full "
                    />
                  </div>
                  <div className=" flex  gap-5 ">
                    <label className="mr-5">PHONE </label>
                    <input
                      type="number"
                      name="phone"
                      value={phone}
                      onChange={this.updateInput}
                      className="outline outline-offset-1 outline-1 focus:ring-4 w-full "
                    />
                  </div>
                  <div className=" flex  gap-5 ">
                    <label className="mr-3">SUBJECT </label>

                    <select className="outline outline-offset-1 outline-1 focus:ring-4 w-full " name="subject" onChange={this.updateInput}>
                      <option value='html' >HTML</option>
                      <option value='css' >CSS</option>
                      <option value='javascript' >Javascript</option>
                    </select>
                  </div>

                  <div>
                    <div>
                      <div>GENDER</div>
                      <div className="ml-20">
                        <div className="flex gap-5  ">
                          <input className="" type="checkbox" value="Male" onChange={this.updateCheckbox} />
                          <label htmlFor="">Male</label>
                        </div>
                        <div className="flex gap-5  ">
                          <input className="" type="checkbox" value="Female" onChange={this.updateCheckbox} />
                          <label htmlFor="">Female</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <button className="px-4 py-2 bg-violet-400 rounded-lg text-white">
                  Submit
                </button>
              </div>
            </form>

            <div className="bg-gray-500 text-white mt-20 p-5 rounded-lg">
              <table className="w-full table-auto border-separate  border-spacing-4 border border-slate-500 ">
                <thead>
                  <tr>
                    <th >First Name</th>
                    <th >Last Name</th>
                    <th>Company</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Subject</th>
                    <th>Gender</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    records.length === 0 && (
                      <tr>
                        <td>NO Record</td>
                      </tr>
                    )
                  }
                  {

                    records.map((record, index) => (
                      <tr key={index}>
                        <td>{record.fname}</td>
                        <td>{record.lname}</td>
                        <td>{record.company}</td>
                        <td>{record.email}</td>
                        <td>{record.phone}</td>
                        <td>{record.subject}</td>
                        <td>{record.gender}</td>
                      </tr>
                    ))
                  }

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
