import React from 'react'

const SubmitForm = () => {
  return (
    <div>
      <form>
        <div>

          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              gap: '5px'
            }}
          >
            <label>Email</label>
            <input
              style={{
                width: '300px',
                padding: '5px'
              }}
              type="email" />
          </div>

          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              gap: '5px'
            }}
          >
            <label>Password</label>
            <input
              style={{
                width: '300px',
                padding: '5px'
              }}
              type="password" />
          </div>

        </div>

        <div
          style={{
            display: 'flex',
            gap: '20px',
            padding: '5px 0px'
          }}
        >
          <div>
            <input type="checkbox" />
            <label>remember me</label>
          </div>
          <div>
            <input type="checkbox" />
            <label>Forgot password</label>
          </div>

        </div>

        <button>Submit</button>

      </form>
    </div>
  )
}

export default SubmitForm