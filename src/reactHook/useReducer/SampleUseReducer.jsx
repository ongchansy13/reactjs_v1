import React, { useReducer } from "react";

const reducer = (state, action) => {
  switch (action.type) {
    case "increment":
      return {
        count: state.count + 1,
      };
    case "decrement":
      if (state.count === 0) {
        return {
          ...state,
          count: (state.count = 0),
        };
      } else {
        return {
          ...state,
          count: state.count - 1,
        };
      }
    case 'newUserInput':
      return {
        ...state,
        id: state.length + 1,
        userInput: action.payload,

      };
    case 'changeColor':
      return {
        ...state,
        color: !state.color,
      }
    case 'delete':
      return {
        ...state,

      }

    default:
      return state;
  }
};

const SampleUseReducer = () => {
  const [state, dispatch] = useReducer(reducer, [{
    id: null,
    count: 0,
    userInput: "",
    color: false,
  }]);

  return (
    <div className={`max-w-sm mx-auto bg-white shadow-xl p-5 grid items-center justify-center ${state.color ? 'text-gray-500' : 'text-black'}`}>
      <div>
        <input
          type="text"
          className="border-2 border-slate-300 p-2"
          value={state.userInput}
          onChange={(e) =>
            dispatch({ type: "newUserInput", payload: e.target.value })
          }
        />
      </div>

      <h1 className="px-5 py-3 outline outline-slate-300 shadow justify-self-center mt-5">
        {state.count}
      </h1>

      <div className="flex justify-center gap-5">
        <button
          className="text-lg border px-5 py-2 mt-5"
          onClick={() => dispatch({ type: "decrement" })}
        >
          -
        </button>
        <button
          className="text-lg border px-5 py-2 mt-5"
          onClick={() => dispatch({ type: "increment" })}
        >
          +
        </button>
        <button className="text-lg border px-5 py-2 mt-5" onClick={() => dispatch({ type: 'changeColor' })}>color</button>
      </div>

      <h2>{state.userInput}</h2>
    </div>
  );
};

export default SampleUseReducer;
