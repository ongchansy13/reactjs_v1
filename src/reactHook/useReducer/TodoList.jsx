import React from "react";
import { ACTIONS } from "../useReducer/Todos";

const TodoList = ({ todo, dispatch }) => {
  return (
    <div>
      <span style={{ color: todo.completed ? "#AAA" : "#000" }}>
        {todo.name}
      </span>
      <button
        onClick={() =>
          dispatch({ type: ACTIONS.TOGGLE_TODO, payload: { id: todo.id } })
        }
        className="border px-2 py-1 bg-violet-300 rounded-lg "
      >
        toggle
      </button>
      <button
        onClick={() =>
          dispatch({ type: ACTIONS.DELETE_TODO, payload: { id: todo.id } })
        }
        className="border px-2 py-1 bg-violet-300 rounded-lg "
      >
        delete
      </button>
    </div>
  );
};

export default TodoList;
