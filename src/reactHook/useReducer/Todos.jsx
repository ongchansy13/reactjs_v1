import React, { useReducer, useState } from "react";
import TodoList from "./TodoList";

export const ACTIONS = {
  TODO: "todos",
  TOGGLE_TODO: "toggle_todo",
  DELETE_TODO: "delete_todo",
};

const Todos = () => {
  const reducer = (todo, action) => {
    switch (action.type) {
      case ACTIONS.TODO: {
        return [...todo, newTodo(action.payload.name)];
      }
      case ACTIONS.TOGGLE_TODO: {
        return todo.map((t) => {
          if (t.id === action.payload.id) {
            return {
              ...t,
              completed: !t.completed,
            };
          }
          return t;
        });
      }
      case ACTIONS.DELETE_TODO: {
        return todo.filter((t) => {
          return t.id !== action.payload.id;
        });
      }
      default: {

        return todo;
      }
    }
  };

  const newTodo = (name) => {
    return {
      id: Math.random(),
      name: name,
      completed: false,
    };
  };

  const [todo, dispatch] = useReducer(reducer, []);
  const [name, setName] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({
      type: ACTIONS.TODO,
      payload: { name },
    });
    setName("");
  };

  console.log(todo);

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          className="outline outline-red-100 ring-offset-1 "
          type="text"
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </form>

      {todo.map((t) => {
        return <TodoList key={t.id} todo={t} dispatch={dispatch} />;
      })}
    </div>
  );
};

export default Todos;
