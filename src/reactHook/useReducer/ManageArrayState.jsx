import React, { useReducer } from 'react'

const initialState = {
  names: [],
  name: '',
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'setName':
      return {
        ...state,
        name: action.payload,
      }
    case 'addName':
      return {
        ...state,
        names: [...state.names, state.name],
        name: '',
      }
    default:
      return state
  }
}

const ManageArrayState = () => {

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div>
      <h1>Hello World</h1>
      <h2>
        {
          state.names.map((name, index) => {
            return <div key={index}>{name}</div>
          })
        }
      </h2>
      <form>
        <input type="text" value={state.name} onChange={(e) => dispatch({ type: 'setName', payload: e.target.value })} />
      </form>

      <button onClick={() => dispatch({ type: 'addName' })}>add</button>


    </div>
  )
}

export default ManageArrayState