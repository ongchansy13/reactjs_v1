import React, { useReducer } from 'react'

const initialState = {
  name: 'jane',
  age: 20,
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'incrementAge':
      return {
        ...state,
        name: state.name,
        age: state.age + 1,
      }
    case 'changeName':
      return {
        ...state,
        name: action.nextName,
        age: state.age,
      }
    default:
      throw Error('Unknown action', action.type)
  }
}

const ManageStateObject = () => {

  const [state, dispatch] = useReducer(reducer, initialState);

  const clickAge = () => {
    dispatch({
      type: 'incrementAge',
    })
  }

  const updateName = (e) => {
    dispatch({
      type: 'changeName',
      nextName: e.target.value,
    })
  }

  return (
    <div>
      <input type="text" value={state.name} onChange={updateName} />
      <button onClick={clickAge}>increment age</button>

      <p>Hello, {state.name}. You are {state.age} </p>
    </div>
  )
}

export default ManageStateObject