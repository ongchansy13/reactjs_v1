import React, { useState, useEffect } from 'react'


const ReactHook = () => {
  const [count, setCount] = useState(0);
  const [calculate, setCalculatee] = useState(0);

  // useEffect(() => {
  //   setTimeout((prev) => {
  //     setCount((prev) => prev + 1)
  //   }, 1000)
  // }, [])


  // useEffect(() => (
  //   setCalculatee(count * 2)
  // ), [count])

  useEffect(() => {
    let timer = setTimeout(() => {
      setCount((prev) => prev + 1)
    }, 100)
    return () => clearTimeout(timer);
  }, [])

  return (
    <div>
      <h1>{count}</h1>
      <h1>{calculate}</h1>
      <button onClick={() => setCount((prev) => prev + 1)}>click</button>
    </div>
  )
}

export default ReactHook