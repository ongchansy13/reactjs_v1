import React, { useState, createContext, useContext } from 'react'

const UserContext = createContext();

const UseContext = () => {
  const [user, setUser] = useState('jane');
  return (
    <UserContext.Provider value={user}>
      <h1>Hello {user}</h1>
      <Component2 />
    </UserContext.Provider>
  )
}

const Component2 = () => {
  return (
    <div>
      <h1>Hello component2 </h1>
      <Component3 />
    </div>
  )
}

const Component3 = () => {
  return (
    <div>
      <h1>Hello component 3 </h1>
      <Component4 />
    </div>
  )
}

const Component4 = () => {
  return (
    <div>
      <h1>Hello component 4 </h1>
      <Component5 />
    </div>
  )
}

const Component5 = () => {
  const user = useContext(UserContext);
  return (
    <div>
      <h1>Hello component 5 {user}</h1>
    </div>
  )
}

export default UseContext