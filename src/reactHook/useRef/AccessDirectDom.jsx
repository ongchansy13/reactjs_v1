import React, { useRef } from 'react'

const AccessDirectDom = () => {
  const inputRef = useRef();

  const clickHandler = () => {
    inputRef.current.focus();
  }
  return (
    <div>
      <h2>AccessDirectDom</h2>

      <input type="text" ref={inputRef} />
      <button onClick={clickHandler}>Click</button>
    </div>
  )
}

export default AccessDirectDom